package com.devcamp.booauthor.bookcontroller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.Author;
import com.devcamp.model.book;


@RestController
@RequestMapping("/")
@CrossOrigin
public class bookcontroller {
    @GetMapping("/books")

    public ArrayList<book> geAuthors(){
        ArrayList<book> books = new ArrayList<book>();
        Author author1 = new Author("cuong", "cuong@gmail.com",'m'); 
        Author author2 = new Author("nam", "cuong@gmail.com",'m');
        Author author3 = new Author("nguyen", "cuong@gmail.com",'m');

        System.out.println(author1.toString());
        System.out.println(author2.toString());
        System.out.println(author3.toString());


        book book1 = new book("chien tranh va hoa binh", author3, 500000, 1);
        book book2= new book("cuop bien caribe", author1, 500000, 1);
        book book3 = new book("xac song", author2, 500000, 1);

        System.out.println(book1.toString());
        System.out.println(book2.toString());
        System.out.println(book3.toString());

        books.add(book1);
        books.add(book2);
        books.add(book3);
        return books;
    }
}

package com.devcamp.booauthor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooauthorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooauthorApplication.class, args);
	}

}
